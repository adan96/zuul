package com.route.zuul.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

public class ProxyFilter extends ZuulFilter {


    @Override
    public String filterType() {//Tipo de filtro
        return "pre";
    }

    @Override
    public int filterOrder() {//Orden para ejecutar
        return 1;
    }

    @Override
    public boolean shouldFilter() {//Si o no se filtra
        boolean isMobile = false;
        RequestContext ctx = getCurrentContext();
        String param = ctx.getRequest().getParameter("source");
        if(param != null && param.equals("mobile"))
            isMobile = true;
        return isMobile;
    }

    @Override
    public Object run() throws ZuulException {//Qué se debe filtrar
        System.out.println("Llamando un filtro");
        return null;
    }

}
